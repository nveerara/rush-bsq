# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nveerara <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/07/06 10:46:36 by nveerara          #+#    #+#              #
#    Updated: 2020/07/23 09:02:29 by nveerara         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= BSQ 
CC		= gcc
FLAGS	= -Wall -Wextra -Werror
SRCDIR 	= srcs/
SRC		= main.c \
		  rg_lst_func.c \
		  l_rg_lst.c \
		  map_error.c \
		  read_fd.c \
		  line_range_list_func.c \
		  range_list_func.c \
		  read_init_bsq.c \
		  read_bsq.c \
		  print_bsq.c \
		  ft_strncpy.c \
		  ft_strnstr.c \
		  read_desc_line.c \
		  print_line_sqr.c

OBJ		= ${SRC:.c=.o}
RM		= rm -f
INCDIR	= includes

all : ${NAME}

%.o :	%.c
	${CC} ${FLAGS} -I ${INCDIR} -c $< -o $@

${NAME} : $(addprefix $(SRCDIR), $(OBJ))
	${CC} ${FLAGS} $(addprefix $(SRCDIR), $(OBJ)) $(addprefix $(LIBDIR), $(LIBS)) -o $@

clean :
	${RM} $(addprefix $(SRCDIR), $(OBJ))

fclean : clean
	${RM} ${NAME}

re : fclean all
