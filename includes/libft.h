/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/08 10:02:46 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 10:36:17 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

int				ft_atoi(char *str);
int				ft_atoi_base(char *str, char *base);
void			*ft_print_memory(void *addr, unsigned int size);
void			ft_putchar(char c);
void			ft_putnbr(int nb);
void			ft_putnbr_base(int nb, char *base);
void			ft_putstr(char *str);
void			ft_putstr_non_printable(char *str);
char			**ft_split(char *str, char *charset);
char			*ft_strcat(char *dest, char *src);
int				ft_strcmp(char *s1, char *s2);
char			*ft_strcpy(char *dest, char *src);
char			*ft_strdup(char *src);
char			*ft_strjoin(int size, char **strs, char *sep);
unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);
unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);
int				ft_strlen(char *str);
char			*ft_strncat(char *dest, char *src, unsigned int nb);
int				ft_strncmp(char *s1, char *s2, unsigned int n);
char			*ft_strncpy(char *dest, char *src, unsigned int n);
char			*ft_strstr(char *str, char *to_find);
char			*ft_strnstr(char *str, char *to_find, unsigned int n);
void			ft_putnbr_base_pad(int nb, char *base, int pad);
void			ft_putnbr_base_pad_u(unsigned int nb, char *base, int pad);
void			ft_putstr_fd(char *str, int fd);

#endif
