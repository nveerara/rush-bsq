/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 18:11:31 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 12:11:49 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include "libft.h"

# define BUFFER_SIZE 4096
# define TMP_FILE "BSQ_TMP"
# define EMPTY 0
# define OBSTACLE 1
# define FILL 2
# define X 0
# define Y 1

typedef struct		s_range_list
{
	int					start;
	int					end;
	int					size;
	struct s_range_list	*next;
}					t_rg_lst;

typedef struct		s_range_mask
{
	t_rg_lst		*first;
	t_rg_lst		*prt;
	t_rg_lst		*rt;
	t_rg_lst		*rp;
}					t_rg_mk;

typedef struct		s_lines_range_lst
{
	t_rg_lst					*first;
	struct s_lines_range_lst	*up;
}					t_l_rg_lst;

typedef struct		s_buffer_v
{
	char			buf[BUFFER_SIZE];
	int				ret;
	int				fd;
	int				stdin_file;
	char			*desc_line;
	int				desc_l_len;
	char			*nl;
}					t_bv;

typedef struct		s_bsq
{
	int				mc[3];
	int				ln_size;
	int				ln_nb;
	int				y;
	int				pos[2];
	int				size;
	t_l_rg_lst		*cl;
	int				cl_size;
}					t_bsq;

typedef struct		s_print_sqr
{
	int				i;
	int				c;
	int				y;
	char			*nl;
	int				tmp;
	char			*l_sqr;
}					t_pt_sqr;

typedef struct		s_read_line
{
	int				i;
	int				start;
	t_rg_lst		*lst;
	t_rg_lst		*elst;
}					t_rdl;

int					read_first_line(t_bsq *bsq, t_bv *bv);

int					read_init(t_bsq *bsq, t_bv *bv);

int					read_bsq_stdin(void);
int					read_bsq_file(char *file);

int					map_error(t_bsq *bsq, t_rdl *rdl, t_bv *bv);
void				free_all(t_bsq *bsq, t_rdl *rdl, t_bv *bv);

void				free_lst_nexts(t_rg_lst *l);
t_rg_lst			*create_range(int start, int end);
void				create_list_rg_nx(int start, int end, t_rg_lst *lst);
void				add_new_link(t_rg_lst **lst
		, t_rg_lst **elst, int start, int end);

t_rg_lst			*ranges_mask(t_rg_lst *rp
		, t_rg_lst *rt, int lc, t_bsq *bsq);

void				delete_rest_line(t_l_rg_lst *l);
void				lines_ranges(t_bsq *bsq);

t_l_rg_lst			*create_line(void);
t_l_rg_lst			*insert_line(t_l_rg_lst *llst);

int					read_bsq(t_bsq *bsq, t_bv *bv);

void				print_bsq(t_bv *bv, t_bsq *bsq);

void				print_line_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt);

#endif
