/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bsq.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/21 22:53:47 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 12:10:53 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void		gen_sqr_line(t_pt_sqr *pt, t_bsq *bsq)
{
	int		i;

	if ((pt->l_sqr = (char *)malloc(sizeof(char) * bsq->size)) == NULL)
		exit(3);
	i = 0;
	while (i < bsq->size)
	{
		pt->l_sqr[i] = bsq->mc[FILL];
		i++;
	}
}

static void		init_print_bsq(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	pt->y = 0;
	if (bsq->size != 0)
		gen_sqr_line(pt, bsq);
	while ((bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE))
			&& (pt->nl = ft_strnstr(bv->buf, "\n", bv->ret)) == NULL)
	{
	}
	bv->ret -= (pt->nl - bv->buf + 1);
	ft_strncpy(bv->buf, pt->nl + 1, bv->ret);
}

static void		print_bef_ln_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	pt->nl = NULL;
	while (pt->y < bsq->pos[Y] &&
			(pt->nl = ft_strnstr(
		(pt->nl == NULL ? bv->buf : (bv->buf +
			(pt->nl - bv->buf + 1)))
			, "\n"
			, (pt->nl == NULL ?
				bv->ret : bv->ret - (pt->nl - bv->buf + 1)))) != NULL)
		pt->y++;
	if (pt->y < bsq->pos[Y])
		write(1, bv->buf, bv->ret);
}

static void		print_bef_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	if (pt->y < bsq->pos[Y])
	{
		print_bef_ln_sqr(bv, bsq, pt);
		while (pt->y < bsq->pos[Y]
				&& (bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
			print_bef_ln_sqr(bv, bsq, pt);
		write(1, bv->buf, pt->nl - bv->buf + 1);
		bv->ret -= (pt->nl - bv->buf + 1);
		ft_strncpy(bv->buf, pt->nl + 1, bv->ret);
	}
}

void			print_bsq(t_bv *bv, t_bsq *bsq)
{
	t_pt_sqr	pt;

	init_print_bsq(bv, bsq, &pt);
	if (bsq->size != 0)
	{
		print_bef_sqr(bv, bsq, &pt);
		pt.i = 0;
		if (pt.y == bsq->pos[Y])
			print_line_sqr(bv, bsq, &pt);
		while (pt.y < bsq->pos[Y] + bsq->size
				&& (bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
			print_line_sqr(bv, bsq, &pt);
		free(pt.l_sqr);
	}
	else
		write(1, bv->buf, bv->ret);
	while ((bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
		write(1, bv->buf, bv->ret);
}
