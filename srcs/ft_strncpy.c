/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/30 20:36:55 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 19:06:49 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	i;
	char			*bdest;

	i = 0;
	bdest = dest;
	if (dest != NULL && src != NULL)
	{
		while (*(src + i) && i < n)
		{
			*(dest + i) = *(src + i);
			i++;
		}
		while (i < n)
		{
			*(dest + i) = '\0';
			i++;
		}
	}
	return (bdest);
}
