/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_line_sqr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/23 08:55:37 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 09:04:58 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void	print_bef_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	pt->tmp = (bv->ret - pt->c < bsq->pos[X] - pt->i
			? bv->ret - pt->c : bsq->pos[X] - pt->i);
	write(1, bv->buf + pt->c, pt->tmp);
	pt->i += pt->tmp;
	pt->c += pt->tmp;
}

static void	print_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	pt->tmp = (bv->ret - pt->c < bsq->pos[X] + bsq->size - pt->i
			? bv->ret - pt->c : bsq->pos[X] + bsq->size - pt->i);
	write(1, pt->l_sqr, pt->tmp);
	pt->c += pt->tmp;
	pt->i += pt->tmp;
}

static void	print_aft_sqr(t_bv *bv, t_pt_sqr *pt)
{
	if ((pt->nl = ft_strnstr(bv->buf + pt->c, "\n", bv->ret - pt->c)))
	{
		write(1, bv->buf + pt->c, pt->nl - (bv->buf + pt->c) + 1);
		pt->i = 0;
		pt->c += (pt->nl - (bv->buf + pt->c) + 1);
		pt->y++;
	}
	else
	{
		write(1, bv->buf + pt->c, bv->ret - pt->c);
		pt->i += bv->ret;
		pt->c += bv->ret;
	}
}

void		print_line_sqr(t_bv *bv, t_bsq *bsq, t_pt_sqr *pt)
{
	pt->c = 0;
	while (pt->c < bv->ret)
	{
		if (pt->i < bsq->pos[X])
			print_bef_sqr(bv, bsq, pt);
		else if (pt->i < bsq->pos[X] + bsq->size
				&& pt->y < bsq->pos[Y] + bsq->size)
			print_sqr(bv, bsq, pt);
		else
			print_aft_sqr(bv, pt);
	}
}
