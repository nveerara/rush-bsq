/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 10:34:31 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 10:35:12 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strnstr(char *str, char *to_find, unsigned int n)
{
	int		i;

	if (*str == '\0' && *to_find == '\0')
		return (str);
	while (*str && n)
	{
		i = 0;
		while (*(to_find + i) == *(str + i) && *(to_find + i) && *(str + i))
			i++;
		if (*(to_find + i) == '\0')
			return (str);
		str++;
		n--;
	}
	return (NULL);
}
