/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   l_rg_lst.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 23:14:30 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/21 19:57:54 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

t_l_rg_lst	*create_line(void)
{
	t_l_rg_lst	*line;

	if ((line = (t_l_rg_lst *)malloc(sizeof(*line))) == NULL)
		exit(3);
	line->up = NULL;
	return (line);
}

t_l_rg_lst	*insert_line(t_l_rg_lst *llst)
{
	t_l_rg_lst	*tmp;

	tmp = create_line();
	tmp->up = llst;
	tmp->first = NULL;
	return (tmp);
}
