/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_bsq.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 18:10:36 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 11:23:34 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void		init_rd_new_line(t_bsq *bsq, t_rdl *rdl)
{
	if (rdl->lst != NULL)
		bsq->cl->first = rdl->lst;
	else
		bsq->cl->first = NULL;
	rdl->i = -1;
	rdl->start = -1;
	rdl->lst = NULL;
	if (bsq->cl->first == NULL)
	{
		delete_rest_line(bsq->cl);
		bsq->cl = NULL;
	}
	else
		lines_ranges(bsq);
	bsq->cl = insert_line(bsq->cl);
	bsq->y++;
}

static void		check_add_new_link(t_bsq *bsq, t_rdl *rdl)
{
	if (0 <= rdl->start)
	{
		if (rdl->i - rdl->start > bsq->size
				&& bsq->y + bsq->size - bsq->cl_size <= bsq->ln_nb)
			add_new_link(&rdl->lst, &rdl->elst, rdl->start, rdl->i);
		rdl->start = -1;
	}
}

static void		init_range(t_rdl *rdl)
{
	if (rdl->start == -1)
		rdl->start = rdl->i;
}

static int		read_buffer_line(t_bsq *bsq, t_bv *bv, t_rdl *rdl)
{
	int c;

	c = 0;
	while (c < bv->ret)
	{
		if (bv->buf[c] == bsq->mc[EMPTY])
			init_range(rdl);
		else if (bv->buf[c] == bsq->mc[OBSTACLE])
			check_add_new_link(bsq, rdl);
		else if (bv->buf[c] == '\n')
		{
			check_add_new_link(bsq, rdl);
			if (rdl->i == bsq->ln_size)
				init_rd_new_line(bsq, rdl);
			else
				return (map_error(bsq, rdl, NULL));
		}
		else
			return (map_error(bsq, rdl, NULL));
		rdl->i++;
		c++;
	}
	return (0);
}

int				read_bsq(t_bsq *bsq, t_bv *bv)
{
	t_rdl	rdl;

	init_rd_new_line(bsq, &rdl);
	rdl.i++;
	if (bv->fd == STDIN_FILENO)
		write(bv->stdin_file, bv->buf, bv->ret);
	if (read_buffer_line(bsq, bv, &rdl))
		return (1);
	while ((bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
	{
		if (bv->fd == STDIN_FILENO)
			write(bv->stdin_file, bv->buf, bv->ret);
		if (read_buffer_line(bsq, bv, &rdl))
			return (1);
	}
	if (rdl.i)
		return (map_error(bsq, &rdl, NULL));
	free_all(bsq, &rdl, NULL);
	return (0);
}
