/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/21 18:08:53 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 11:45:53 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int				read_bsq_stdin(void)
{
	t_bsq	bsq;
	t_bv	bv;

	bv.fd = STDIN_FILENO;
	if ((bv.stdin_file = open(TMP_FILE, O_WRONLY | O_CREAT | O_TRUNC, 0777)) == -1)
		return (map_error(NULL, NULL, NULL));
	else
	{
		if (read_init(&bsq, &bv) || read_first_line(&bsq, &bv)
				|| read_bsq(&bsq, &bv))
			return (1);
		if (bsq.ln_nb == bsq.y)
		{
			close(bv.stdin_file);
			if ((bv.stdin_file = open(TMP_FILE, O_RDONLY)) == -1)
				return (map_error(&bsq, NULL, NULL));
			bv.fd = bv.stdin_file;
			print_bsq(&bv, &bsq);
			close(bv.stdin_file);
		}
		else
			map_error(&bsq, NULL, NULL);
	}
	return (0);
}

int				read_bsq_file(char *file)
{
	t_bsq	bsq;
	t_bv	bv;

	if ((bv.fd = open(file, O_RDONLY)) == -1)
		return (map_error(NULL, NULL, NULL));
	else
	{
		if (read_init(&bsq, &bv) || read_first_line(&bsq, &bv)
				|| read_bsq(&bsq, &bv))
			return (1);
		if (bsq.ln_nb == bsq.y)
		{
			close(bv.fd);
			if ((bv.fd = open(file, O_RDONLY)) == -1)
				return (map_error(&bsq, NULL, NULL));
			print_bsq(&bv, &bsq);
		}
		else
			map_error(&bsq, NULL, NULL);
	}
	close(bv.fd);
	return (0);
}
