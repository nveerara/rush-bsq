/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_init_bsq.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/21 13:09:48 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 08:46:11 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void		check_add_new_link(t_rdl *rdl)
{
	if (0 <= rdl->start)
	{
		add_new_link(&rdl->lst, &rdl->elst, rdl->start, rdl->i);
		rdl->start = -1;
	}
}

static int		read_buffer_first_line(t_bsq *bsq, t_bv *bv, t_rdl *rdl, int *c)
{
	*c = 0;
	while (*c < bv->ret && bsq->ln_size == -1)
	{
		if (bv->buf[*c] == bsq->mc[EMPTY])
		{
			if (rdl->start == -1)
				rdl->start = rdl->i;
		}
		else if (bv->buf[*c] == bsq->mc[OBSTACLE])
			check_add_new_link(rdl);
		else if (bv->buf[*c] == '\n')
		{
			check_add_new_link(rdl);
			bsq->ln_size = rdl->i;
		}
		else
			return (map_error(bsq, rdl, NULL));
		rdl->i++;
		(*c)++;
	}
	return (0);
}

int				read_first_line(t_bsq *bsq, t_bv *bv)
{
	t_rdl	rdl;
	int		c;

	rdl.i = 0;
	rdl.start = -1;
	rdl.lst = NULL;
	bsq->cl = insert_line(bsq->cl);
	if (read_buffer_first_line(bsq, bv, &rdl, &c))
		return (1);
	if (bv->fd == STDIN_FILENO)
		write(bv->stdin_file, bv->buf, c);
	while (bsq->ln_size == -1 && (bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
	{
		if (read_buffer_first_line(bsq, bv, &rdl, &c))
			return (1);
		if (bv->fd == STDIN_FILENO)
			write(bv->stdin_file, bv->buf, c);
	}
	bsq->cl->first = rdl.lst;
	if (bsq->ln_size <= 0)
		return (map_error(bsq, &rdl, NULL));
	ft_strncpy(bv->buf, bv->buf + c, bv->ret - c);
	bv->ret -= c;
	return (0);
}
