/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_desc_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/22 17:53:53 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 11:26:09 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void		join_buf(t_bv *bv)
{
	char *join;

	if (bv->desc_l_len + bv->ret)
	{
		if ((join = (char *)malloc(sizeof(char)
						* (bv->desc_l_len + bv->ret))) == NULL)
			exit(3);
		ft_strncpy(join, bv->desc_line, bv->desc_l_len);
		ft_strncpy(join + bv->desc_l_len, bv->buf, bv->ret);
		if (bv->desc_line != NULL)
			free(bv->desc_line);
		bv->desc_l_len += bv->ret;
		bv->desc_line = join;
	}
}

static int		check_text_file(t_bv *bv)
{
	int i;

	i = 0;
	while (i < bv->ret)
	{
		if (bv->buf[i++] < 1)
			return (1);
	}
	return (0);
}

static int		init_desc_line(t_bv *bv)
{
	bv->desc_line = NULL;
	bv->desc_l_len = 0;
	bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE);
	if (check_text_file(bv))
		return (1);
	join_buf(bv);
	while ((bv->nl = ft_strnstr(bv->buf, "\n", bv->ret)) == NULL
			&& (bv->ret = read(bv->fd, bv->buf, BUFFER_SIZE)))
	{
		if (check_text_file(bv))
			return (1);
		join_buf(bv);
	}
	if (bv->desc_line == NULL)
		return (1);
	return (0);
}

static int		init_lnnb(t_bsq *bsq, t_bv *bv)
{
	int i;

	i = 0;
	bsq->ln_size = -1;
	bsq->mc[FILL] = bv->desc_line[bv->nl - bv->desc_line - 1];
	bsq->mc[OBSTACLE] = bv->desc_line[bv->nl - bv->desc_line - 2];
	bsq->mc[EMPTY] = bv->desc_line[bv->nl - bv->desc_line - 3];
	while (i < bv->nl - bv->desc_line - 3)
	{
		if ('0' <= bv->desc_line[i] && bv->desc_line[i] <= '9')
		{
			bsq->ln_nb *= 10;
			bsq->ln_nb += bv->desc_line[i] - '0';
		}
		else
			return (1);
		i++;
	}
	return (0);
}

int				read_init(t_bsq *bsq, t_bv *bv)
{
	bsq->cl = NULL;
	bsq->size = 0;
	bsq->y = 0;
	bsq->pos[Y] = -1;
	if (init_desc_line(bv))
		return (map_error(bsq, NULL, bv));
	bv->nl = ft_strnstr(bv->desc_line, "\n", bv->desc_l_len);
	if (bv->nl - bv->desc_line < 4)
		return (map_error(bsq, NULL, bv));
	bsq->ln_nb = 0;
	if (init_lnnb(bsq, bv))
		return (map_error(bsq, NULL, bv));
	if (bsq->ln_nb == 0)
		return (map_error(bsq, NULL, bv));
	if (bsq->mc[EMPTY] == bsq->mc[OBSTACLE]
			|| bsq->mc[EMPTY] == bsq->mc[FILL]
			|| bsq->mc[OBSTACLE] == bsq->mc[FILL])
		return (map_error(bsq, NULL, bv));
	if (bv->fd == STDIN_FILENO)
		write(bv->stdin_file, bv->desc_line, bv->nl - bv->desc_line + 1);
	ft_strncpy(bv->buf, bv->nl + 1
			, bv->desc_l_len - (bv->nl - bv->desc_line + 1));
	bv->ret = bv->desc_l_len - (bv->nl - bv->desc_line + 1);
	free(bv->desc_line);
	return (0);
}
