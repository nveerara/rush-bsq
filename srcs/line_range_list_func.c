/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   line_range_list_func.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 21:22:22 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 23:37:31 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void			delete_rest_line(t_l_rg_lst *l)
{
	t_l_rg_lst	*tmp;

	while (l != NULL)
	{
		free_lst_nexts(l->first);
		tmp = l;
		l = l->up;
		free(tmp);
	}
}

static void		add_new_sqr(t_bsq *bsq, t_l_rg_lst *l, int lc)
{
	bsq->pos[X] = l->first->start;
	bsq->pos[Y] = bsq->y - lc + 1;
	bsq->size = lc;
}

void			lines_ranges(t_bsq *bsq)
{
	t_l_rg_lst		*l;
	t_l_rg_lst		*pl;
	int				lc;

	l = bsq->cl;
	lc = 1;
	while (l->up != NULL)
	{
		lc++;
		pl = l;
		l = l->up;
		if ((l->first = ranges_mask(pl->first, l->first, lc, bsq)) == NULL)
		{
			lc--;
			pl->up = NULL;
			delete_rest_line(l);
			l = pl;
		}
	}
	bsq->cl_size = lc;
	if (bsq->size < lc)
		add_new_sqr(bsq, l, lc);
}
