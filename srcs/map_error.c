/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/21 17:31:29 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/23 09:47:36 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	free_all(t_bsq *bsq, t_rdl *rdl, t_bv *bv)
{
	if (bsq != NULL && bsq->cl != NULL)
	{
		delete_rest_line(bsq->cl);
		bsq->cl = NULL;
	}
	if (rdl != NULL)
	{
		free_lst_nexts(rdl->lst);
		rdl->lst = NULL;
	}
	if (bv != NULL)
	{
		if (bv->desc_line != NULL)
			free(bv->desc_line);
	}
}

int		map_error(t_bsq *bsq, t_rdl *rdl, t_bv *bv)
{
	free_all(bsq, rdl, bv);
	write(STDERR_FILENO, "map error\n", 10);
	return (1);
}
