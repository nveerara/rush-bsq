/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   range_list_func.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 19:12:32 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 23:39:24 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

static void		delete_range(t_rg_mk *mk)
{
	if (mk->rt == mk->first)
		mk->first = (mk->rt)->next;
	if (mk->prt != mk->rt)
	{
		(mk->prt)->next = (mk->rt)->next;
		free(mk->rt);
		mk->rt = (mk->prt)->next;
	}
	else
	{
		mk->prt = (mk->rt)->next;
		free(mk->rt);
		mk->rt = mk->prt;
	}
}

static void		apply_mask(t_rg_mk *mk, t_bsq *bsq, int lc)
{
	int tend;

	(mk->rt)->start = ((mk->rp)->start < (mk->rt)->start
			? (mk->rt)->start : (mk->rp)->start);
	tend = (mk->rt)->end;
	(mk->rt)->end = ((mk->rp)->end < (mk->rt)->end ?
			(mk->rp)->end : (mk->rt)->end);
	(mk->rt)->size = (mk->rt)->end - (mk->rt)->start;
	if ((mk->rp)->end < tend && tend - (mk->rp)->end > bsq->size
		&& tend - (mk->rp)->end > lc)
		create_list_rg_nx((mk->rp)->end, tend, mk->rt);
	if ((mk->rt)->size < bsq->size || (mk->rt)->size < lc)
		delete_range(mk);
	else
	{
		mk->prt = (mk->rt);
		if (mk->rt != NULL)
			mk->rt = (mk->rt)->next;
	}
}

static void		delete_rest(t_rg_mk *mk)
{
	if (mk->rt != NULL)
	{
		if (mk->rt == mk->first)
			mk->first = NULL;
		if (mk->rt != mk->prt)
			mk->prt->next = NULL;
		free_lst_nexts(mk->rt);
	}
}

t_rg_lst		*ranges_mask(t_rg_lst *rp, t_rg_lst *rt, int lc, t_bsq *bsq)
{
	t_rg_mk		mk;

	mk.first = rt;
	mk.prt = rt;
	mk.rt = rt;
	mk.rp = rp;
	while (mk.rp != NULL && mk.rt != NULL)
	{
		if (mk.rp->end <= mk.rt->start)
			mk.rp = mk.rp->next;
		else
		{
			if (mk.rt->end <= mk.rp->start
					|| mk.rt->size < lc
					|| mk.rt->size <= bsq->size)
				delete_range(&mk);
			else
				apply_mask(&mk, bsq, lc);
		}
	}
	delete_rest(&mk);
	return (mk.first);
}
