/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rg_lst_func.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nveerara <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/20 20:56:07 by nveerara          #+#    #+#             */
/*   Updated: 2020/07/22 23:48:57 by nveerara         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

t_rg_lst	*create_range(int start, int end)
{
	t_rg_lst	*range;

	if ((range = (t_rg_lst *)malloc(sizeof(*range))) == NULL)
		exit(3);
	range->start = start;
	range->end = end;
	range->size = end - start;
	range->next = NULL;
	return (range);
}

void		create_list_rg_nx(int start, int end, t_rg_lst *lst)
{
	t_rg_lst	*tmp;

	tmp = create_range(start, end);
	tmp->next = lst->next;
	lst->next = tmp;
}

void		add_new_link(t_rg_lst **lst, t_rg_lst **elst, int start, int end)
{
	if (*lst == NULL)
	{
		*lst = create_range(start, end);
		*elst = *lst;
	}
	else
	{
		(*elst)->next = create_range(start, end);
		*elst = (*elst)->next;
	}
}

void		free_lst_nexts(t_rg_lst *l)
{
	t_rg_lst *tmp;

	while (l != NULL)
	{
		tmp = l;
		l = l->next;
		free(tmp);
	}
}
