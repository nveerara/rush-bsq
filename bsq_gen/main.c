#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER_SIZE 4096

void genline(char *buffer, int x, int rd)
{
	int rds;
	int buf_s;

	rds = rd * x / BUFFER_SIZE;
	buf_s = x;
	while (x)
	{
		x--;
		if (rand() % buf_s < rds)
			buffer[x] = '.';
		else
			buffer[x] = 'o';
	}
}

int main(int ac, char **av)
{
	char buffer[BUFFER_SIZE];
	int xval;
	int yval;
	int x;
	int y;
	int xrest;
	int yrest;
	int64_t rd;


	srand(time(NULL));
	if (ac == 4)
	{
		yval = atoi(av[1]);
		xval = atoi(av[2]);
		x = xval / BUFFER_SIZE;
		xrest = xval % BUFFER_SIZE;
		y = yval;
		yrest = yval % BUFFER_SIZE;
		write(1, av[1], strlen(av[1]));
		write(1, ".ox\n", 4);
		rd = atoi(av[3]);
		rd = rd * BUFFER_SIZE / xval;
		while (y)
		{
			x = xval / BUFFER_SIZE;
			while (x)
			{
				genline(buffer, BUFFER_SIZE, rd);
				write(1, buffer, BUFFER_SIZE);
				x--;
			}
			genline(buffer, xrest, rd);
			write(1, buffer, xrest);
			write(1, "\n", 1);
			y--;
		}
	}
}
